// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "project.h"
#include "projectGameMode.h"
#include "projectHUD.h"
#include "RunnerCharacter.h"

AprojectGameMode::AprojectGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Character/RunnerCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AprojectHUD::StaticClass();
}
